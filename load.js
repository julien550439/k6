import http from 'k6/http';
import { check } from 'k6';

export const options = {
    thresholds: {
        http_req_failed: ['rate<0.02'],
        http_req_duration: ['p(95)<180']
    },
    stages: [
        { duration: '5s', target: 1 },
        { duration: '20s', target: 50 },
        { duration: '30s', target: 100 },
        { duration: '10s', target: 20 },
        { duration: '5s', target: 15000 },
        { duration: '100s', target: 15000 }
    ]
}
export default function() {
    const res= http.get('http://localhost:8080/rest/category/api/allCategories')
    check(res, {'status 200': (r)=> r.status == 200 })
}