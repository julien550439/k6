# k6

## Installation

https://k6.io/docs/get-started/installation/

## Requirements

docker run -p 8080:8080 jbinard/calms-backend

## Run

k6 run load.js

## Report Example 

![report](report.png)